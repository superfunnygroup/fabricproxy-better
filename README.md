# FabricProxy-Better

A fork of FabricProxy-Lite which allows offline-mode players to connect to an offline-mode server while online-players are connected to a online-mode Velocity proxy.
The ip address of each offline players needs to be written in the file config/offline-ip-list.txt, each ip in a seperate line.

If an offline-player joins the server (without Velocity), he must have his ip in the file.
If he doesn't, he will get disconnected.

The online players can have their skins, offline players can view online players's skins.
But offline players cannot have skins without some other mods.

Same as [FabricProxy](https://github.com/OKTW-Network/FabricProxy) but only support velocity and using Fabric-API handle
velocity packet.

This will have the better compatibility with other mods.

## Important

**LuckPerms need enable `hackEarlySend` in config.**

Because Fabric-API can't send packet before QUERY_START event, so player info(UUID) will not ready at QUERY_START event.

Enable `hackEarlySend` will use mixin for early send packet to velocity.

## Setup

* Download mod
* Start server to generate config
* Setting velocity `player-info-forwarding-mode` to `modern` and `forwarding-secret`
* Setting `secret` in `config/Fabric-Lite.toml` match velocity config
